#include <assert.h>

#include "test.h"
#include "util.h"
#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 10

static void success_malloc_test(void* heap_start) {
    console_log("\nSuccess malloc test started..\nInitial heap state:\n");
    debug_heap(stdout, heap_start);

    const size_t block_size = 50;
    void* block = _malloc(block_size);

    console_log("Heap state after memory allocation:\n");
    debug_heap(stdout, heap_start);

    _free(block);

    console_log("Heap state after memory cleaning:\n");
    debug_heap(stdout, heap_start);
} 

static void one_block_free_test(void* heap_start) {
    console_log("\nFree one block test started..\nInitial heap state:\n");
    debug_heap(stdout, heap_start);

    const size_t first_block_size = 3000; 
    const size_t third_block_size = 1000;

    void* first_block = _malloc(first_block_size);
    console_log("Heap state after 1 block allocations:\n");
    debug_heap(stdout, heap_start);

    void* second_block = _malloc(sizeof(uint32_t));
    void* third_block = _malloc(third_block_size);

    console_log("Heap state after 3 block allocations:\n");
    debug_heap(stdout, heap_start);

    _free(second_block);
    console_log("Heap state after cleaning the second block:\n");
    debug_heap(stdout, heap_start);

    _free(third_block);
    _free(first_block);

    debug_heap(stdout, heap_start);
}

static void two_blocks_free_test(void* heap_start) {
    console_log("\nFree two blocks test started..\nInitial heap state:\n");
    debug_heap(stdout, heap_start);

    const size_t first_block_size = 50; 
    const size_t third_block_size = 100;

    void* first_block = _malloc(first_block_size);
    void* second_block = _malloc(sizeof(uint32_t)); 
    void* third_block = _malloc(third_block_size);

    console_log("Heap state after 3 block allocations:\n");
    debug_heap(stdout, heap_start);

    _free(second_block);
    _free(third_block);

    console_log("Heap state after cleaning two last blocks:\n");
    debug_heap(stdout, heap_start);

    _free(first_block);
    debug_heap(stdout, heap_start);
}

static void old_region_expansion_test(void* heap_start) {
    console_log("\nOld region expansion test started..\nInitial heap state:\n");
    debug_heap(stdout, heap_start);
    
    const size_t first_block_size = 7000;
    const size_t second_block_size = 2000;

    void* first_block = _malloc(first_block_size);
    console_log("Heap state after memory allocation:\n");
    debug_heap(stdout, heap_start);

    void* second_block = _malloc(second_block_size);
    console_log("Heap state after growing:\n");
    debug_heap(stdout, heap_start);

    _free(first_block);
    _free(second_block);
    console_log("Heap state after memory cleaning:\n");
    debug_heap(stdout, heap_start);
}

static void new_region_alloc_test(void* heap_start) {
    console_log("\nNew region allocation test started..\nInitial heap state:\n");
    debug_heap(stdout, heap_start);

    const size_t first_block_size = 4000;
    const size_t second_block_size = 300;

    void* first_block = _malloc(first_block_size);
    console_log("Heap state after memory allocation:\n");
    debug_heap(stdout, heap_start);

    const size_t busy_region_size = 8000; 
    map_pages(heap_start, busy_region_size, MAP_SHARED);
    console_log("Heap state after filling:\n");
    debug_heap(stdout, heap_start);

    void* second_block = _malloc(second_block_size);
    console_log("Heap state after second block allocation:\n");
    debug_heap(stdout, heap_start);

    _free(first_block);
    _free(second_block);
    console_log("Heap state after memory cleaning:\n");
    debug_heap(stdout, heap_start);
} 

void run_tests() { 
    void* heap_start = heap_init(HEAP_SIZE);

    success_malloc_test(heap_start);
    one_block_free_test(heap_start);
    two_blocks_free_test(heap_start);
    old_region_expansion_test(heap_start);
    new_region_alloc_test(heap_start);
}
